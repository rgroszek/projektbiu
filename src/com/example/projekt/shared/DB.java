package com.example.projekt.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.projekt.shared.model.Task;

public class DB {
private List<Task> tasks;
    /*
	public DB(){
		tasks = new ArrayList<Task>();
		List<Task>Task1 = new ArrayList<Task>();
		List<Task>Task2 = new ArrayList<Task>();
		List<Task>Task3 = new ArrayList<Task>();
		
		Task1.add(new Task(1, "Zadanko", "Wyniesc smieci", "data"));
		//Task1.add(new Task(2, 1.50, new Date(2014-1900, 05, 15)));
		//Task2.add(new Task(1, 100.00, new Date(2014-1900, 04, 05)));
		//Task3.add(new Task(1, 250.00, new Date(2014-1900, 04, 14)));
		
		//addDebtor(new Debtor("Artur", "Janicki", 1982), debts1);
		//(new Debtor("Maciej", "Nowicki", 1986), debts2);
		//addDebtor(new Debtor("Marcin", "Kowalewski", 1991), debts3);
		
	}
	
	public void addDebtor(Debtor debtor){
		debtors.add(debtor);
	}
	
	public void addDebtor(Debtor debtor, List<Debt> debts){
		debtor.setDebts(debts);
		debtors.add(debtor);
	}
	
	public void delDebtor(Debtor debtor){
		for (Debtor d: debtors){
			if (d.getPersonalID() == debtor.getPersonalID()){
				debtors.remove(d);
			}
		}
	}
	
	public Debtor getDebtor(String perID){
		Debtor debtor = null;
		for (Debtor d: debtors){
			if (d.getPersonalID() == perID){
				debtor = d;
			}
		}
		return debtor;
	}
	
	public List<Debtor> getAllDebtors(){
		return debtors;
	}
	
	public List<Debt> getDebtorsDebts(Debtor debtor){
		List<Debt>debtordebts = debtor.getDebts();
		return debtordebts;
	}

	public void addDebtToDebtor(Debt debt, Debtor debtor) {
		debtor.getDebts().add(debt);
	}

	public void updateDebtor(Debtor debtor, String fName, String lName, int yob) {
		for (Debtor d: debtors){
			if (d.getPersonalID() == debtor.getPersonalID()){
				d.setFirstName(fName);
				d.setLastName(lName);
				d.setYob(yob);
				Integer yearob = yob;
				d.setPersonalID(fName.substring(0, 3).toLowerCase() + lName.substring(0, 3).toLowerCase() + yearob.toString());
			}
		}
	}

	public void returnDebt(Debtor debtor, int id) {
		List<Debt> debts = debtor.getDebts();
		for (Debt d: debts){
			if (d.getId() == id){
				d.setReturned(true);
			}
		}
	}

	public void editDebt(double amount, Date term, Debtor debtor, Debt debt) {
		List<Debt> debts = debtor.getDebts();
		for (Debt d: debts){
			if (d.getId() == debt.getId()){
				d.setAmount(amount);
				d.setTerm(term);
			}
		}
	}
	*/
}
