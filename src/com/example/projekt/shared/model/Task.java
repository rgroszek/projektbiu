package com.example.projekt.shared.model;

public class Task {
	private Long id;
	private String Name;
	private String Content;
	private String DeadLineDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	public String getContent() {
		return Content;
	}
	public void setContent(String content) {
		Content = content;
	}
	
	public String getDeadLineDate() {
		return DeadLineDate;
	}
	public void setDeadLineDate(String deadLineDate) {
		DeadLineDate = deadLineDate;
	}
}
