package com.example.projekt.client.presenter;

import java.util.Date;

import com.example.projekt.client.StorageService;
import com.example.projekt.client.event.TaskDetailsEvent;
import com.example.projekt.client.view.AddTaskView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.Label;

public class AddTaskPresenter implements Presenter{
	final HandlerManager eventBus;
	final StorageService storage;
	private final Display display;

	public interface Display {
		HasClickHandlers getShowTasksButton();
		HasClickHandlers getaddButton();
		Widget asWidget();
		HasValueChangeHandlers<Date> getDataButton();
		HasText deadLineDateLabel();
	}

	public AddTaskPresenter(StorageService storage, HandlerManager eventBus,
			Display view) {
		this.eventBus = eventBus;
		this.storage = storage;
		this.display = view;
		bind();
	}

	@Override
	public void go(HasWidgets container) {
		bind();
		container.clear();
		container.add(display.asWidget());
	}

	private void bind() {
		display.getShowTasksButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new TaskDetailsEvent());
			}
		});
		
		display.getaddButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				
				//eventBus.fireEvent(new TaskDetailsEvent());
			}
		});
		
		display.getDataButton().addValueChangeHandler(new ValueChangeHandler<Date>() {

			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				Date date = event.getValue();
		        String dateString = DateTimeFormat.getMediumDateFormat().format(date);
		        display.deadLineDateLabel().setText(dateString);
				
			}

			//@Override
			/*
			public void onValueChange(ClickEvent event) {
				//eventBus.fireEvent(new TaskDetailsEvent());
				
				display.deadLineDateLabel().setText(" kliknales date typie ty ");
				    
			}
			*/
			
		});
		
		
		
	}
}
