package com.example.projekt.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface TaskDetailsEventHandler extends EventHandler{
	void onTaskDetails(TaskDetailsEvent event);
}
