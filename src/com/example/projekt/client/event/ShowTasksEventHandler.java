package com.example.projekt.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowTasksEventHandler extends EventHandler{
	void onShowTasks(ShowTasksEvent event);
}
