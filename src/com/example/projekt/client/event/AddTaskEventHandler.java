package com.example.projekt.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AddTaskEventHandler extends EventHandler{
	void onAddTask(AddTaskEvent event);
}
