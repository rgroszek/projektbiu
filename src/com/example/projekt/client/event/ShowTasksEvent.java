package com.example.projekt.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ShowTasksEvent extends GwtEvent<ShowTasksEventHandler>{
	public static Type<ShowTasksEventHandler> TYPE = new Type<ShowTasksEventHandler>();
	
	@Override
	public Type<ShowTasksEventHandler> getAssociatedType() {
		return TYPE;
	}
	
	@Override
	protected void dispatch(ShowTasksEventHandler handler) {
		handler.onShowTasks(this);
	}
}
