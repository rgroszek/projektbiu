package com.example.projekt.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class TaskDetailsEvent extends GwtEvent<TaskDetailsEventHandler>{
	public static Type<TaskDetailsEventHandler> TYPE = new Type<TaskDetailsEventHandler>();

	@Override
	public Type<TaskDetailsEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(TaskDetailsEventHandler handler) {
		handler.onTaskDetails(this);
	}
}
