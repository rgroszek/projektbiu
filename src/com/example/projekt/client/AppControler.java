package com.example.projekt.client;

import com.example.projekt.client.event.AddTaskEvent;
import com.example.projekt.client.event.AddTaskEventHandler;
import com.example.projekt.client.event.TaskDetailsEvent;
import com.example.projekt.client.event.TaskDetailsEventHandler;
import com.example.projekt.client.event.ShowTasksEvent;
import com.example.projekt.client.event.ShowTasksEventHandler;
import com.example.projekt.client.presenter.AddTaskPresenter;
import com.example.projekt.client.presenter.TaskDetailsPresenter;
import com.example.projekt.client.presenter.TasksPresenter;
import com.example.projekt.client.presenter.Presenter;
import com.example.projekt.client.view.AddTaskView;
import com.example.projekt.client.view.TaskDetailsView;
import com.example.projekt.client.view.TasksView;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;


public class AppControler implements Presenter, ValueChangeHandler<String> {
	private final HandlerManager eventBus;
	private final StorageService storage;
	private HasWidgets container;

	private final static String SHOW_TASKS = "showTasks";
	private final static String ADD_TASK = "addTask";
	private final static String TASK_DETAILS = "taskDetails";

	public AppControler(StorageService storage, HandlerManager eventBus) {
		this.eventBus = eventBus;
		this.storage = storage;
		bind();
	}
	
	@Override
	public void go(HasWidgets container) {
		this.container = container;

		if ("".equals(History.getToken())) {
			History.newItem(ADD_TASK);
		} else {
			History.fireCurrentHistoryState();
		}
	}
	
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {

		String token = event.getValue();

		if (token != null) {
			Presenter presenter = null;

			if (token.equals(SHOW_TASKS)) {
				presenter = new TasksPresenter(storage, eventBus,
						new TasksView());
			} else if (token.equals(ADD_TASK)) {
				presenter = new AddTaskPresenter(storage, eventBus,
						new AddTaskView());
			} else if (token.equals(TASK_DETAILS)) {
				presenter = new TaskDetailsPresenter(storage, eventBus,
						new TaskDetailsView());
			}

			if (presenter != null) {
				presenter.go(container);
			}
		}
	}
	
	private void bind() {
		History.addValueChangeHandler(this);

		eventBus.addHandler(AddTaskEvent.TYPE, new AddTaskEventHandler() {
			@Override
			public void onAddTask(AddTaskEvent event) {
				doAddTask();
			}
		});

		eventBus.addHandler(TaskDetailsEvent.TYPE,
				new TaskDetailsEventHandler() {

					@Override
					public void onTaskDetails(TaskDetailsEvent event) {
						doTaskDetails();

					}
				});

		eventBus.addHandler(ShowTasksEvent.TYPE,
				new ShowTasksEventHandler() {
					@Override
					public void onShowTasks(ShowTasksEvent event) {
						doShowTasks();
					}
				});
	}

	private void doAddTask() {
		History.newItem(ADD_TASK);
	}
	
	private void doShowTasks() {
		History.newItem(SHOW_TASKS);
	}

	private void doTaskDetails() {
		History.newItem(TASK_DETAILS);
	}
}
