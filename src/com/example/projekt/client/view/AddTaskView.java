package com.example.projekt.client.view;

import java.awt.TextArea;
import java.util.Date;

import com.example.projekt.client.presenter.AddTaskPresenter.Display;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.datepicker.client.DatePicker;

public class AddTaskView extends Composite implements Display {
	private final Button showTasksButton;
	private final Button addButton;
	private final TextBox name;
	private final TextBox content;
	public final Label deadLineDate;
	public final DatePicker callendar;
	
	public AddTaskView() {

		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		showTasksButton = new Button("Show Tasks");
		name = new TextBox();
		name.setText("Wpisz nazwe");
		addButton = new Button ("Add");
		content = new TextBox();
		content.setText("Wpisz treść zadania");
		deadLineDate = new Label("Data");
		callendar = new DatePicker();
		
		VerticalPanel vp1 = new VerticalPanel();
		vp1.add(name);
		vp1.add(content);
		vp1.add(deadLineDate);
		
		HorizontalPanel hp1 = new HorizontalPanel();
		hp1.add(vp1);
		hp1.add(callendar);
		
		mainPanel.add(showTasksButton);
		mainPanel.add(hp1);
		mainPanel.add(addButton);
		
	}
	
	@Override
	public HasClickHandlers getShowTasksButton() {
		return showTasksButton;
	}
	
	@Override
	public HasClickHandlers getaddButton() {
		return addButton;
	}
	
	@Override
	public HasValueChangeHandlers<Date> getDataButton() {
		return  callendar;
	}
	
	@Override
	public HasText deadLineDateLabel() {
		return deadLineDate;
	}
	
	@Override
	public Widget asWidget(){
		return this;
	}

	
}
