package com.example.projekt.client.view;

import com.example.projekt.client.presenter.TaskDetailsPresenter.Display;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class TaskDetailsView extends Composite implements Display{
	private final Button personDetailsButton;
	
	public TaskDetailsView() {

		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		personDetailsButton = new Button("Task details");
		mainPanel.add(personDetailsButton);
	}

	@Override
	public HasClickHandlers getPersonDetailsButton() {
		return personDetailsButton;
	}

	@Override
	public Widget asWidget(){
		return this;
	}
}
