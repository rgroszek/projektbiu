package com.example.projekt.client;

import java.util.ArrayList;
import java.util.List;

import com.example.projekt.shared.model.Task;

public class StorageService {
	private List<Task> db = new ArrayList<Task>();
	  
	  public void addTask(Task task){
		  Task p = new Task();
		  p.setId(task.getId());
		  p.setName(task.getName());
		  p.setContent(task.getContent());
		  p.setDeadLineDate(task.getDeadLineDate());
		  db.add(p);
	  }
	  
	  public List<Task> getAllTasks(){
		  return db;
	  }
}
